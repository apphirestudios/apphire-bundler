path = require 'path'
fs = require 'fs'
async = require 'co'

debug = (set)->
  if FS.debug
    console.log set  
module.exports = FS =
	rootPath: undefined
	_getAbsolutePath: (filePath)->
		if not @rootPath then throw new Error 'VFS root path has not been set'
		return path.join(@rootPath, filePath)
		
	readFile: (filePath, encoding)-> async =>
		debug 'READ ' + @_getAbsolutePath(filePath)
		return fs.readFileSync @_getAbsolutePath(filePath), encoding
		
	stat: (filePath)-> async => 
		debug 'STAT ' + @_getAbsolutePath(filePath)
		return fs.statSync @_getAbsolutePath(filePath)

	internalStat: (filePath)-> async =>
		debug 'INTERNALSTAT ' + @_getAbsolutePath(filePath)
		try
			s = fs.statSync @_getAbsolutePath(filePath)
			if s.isFile() then return 0
			if s.isDirectory() then return 1 
			return -1 #probably device or something
		catch e
			return -1