`Buffer = require('buffer').Buffer`
`process = require('_process')`

global.Object = Object
global.String = String
global.Array = Array
global.Function = Function
global.Date = Date
global.Error = Error
global.Math = Math
global.RegExp = RegExp
global.TypeError = TypeError