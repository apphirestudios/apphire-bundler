path = require 'path'
detective = require 'detective'
async = require 'co'

module.exports = Bundler = 
  debug: false
  vfs: undefined
  rootPaths: ['/']
  sourceMaps: true
  
  ###
    Returns so called module specification

    result is an object containing absolute path of this module, compiled(!) module content and hash of all the dependencies of this module
    
    IMPORTANT NOTICE

    Since we use virtual file systems, returned file path is NOT equals to the real path of the file (even when we use the bundler in node)

    For example if we have file located in /home/user/my-app/src/app.js and we've set VFS's root path to /home/user/my-app
    then returned filename will be /src/app.js
    This is needed for being able to bundle in browser too (with using appropriate VFS adapter)

    @param {String}
    @return {Object}
    
  ###

  getModuleSpec: (fileOrModuleName)-> async =>
    if not fileOrModuleName then throw new Error 'File or module name must be provided'
    if typeof fileOrModuleName isnt 'string' then throw new Error 'File or module name must be a string'
    if not @vfs then throw new Error('VFS for the bundler has not been registered')
    debug "Requesting module spec of #{fileOrModuleName}"
    if fileOrModuleName[0] is '.' then fileOrModuleName = fileOrModuleName.substring(1) #Ensure that path is always absolute'like  
    if path.extname(fileOrModuleName) 
      filename = fileOrModuleName    
    else
      filename = yield @_resolveModulePath(fileOrModuleName)
    debug "Analyzing module from file #{filename}"      
    source = yield Bundler.vfs.readFile(filename, 'utf8')
    extension = path.extname(filename) or '.js'
    if not Bundler._extensions[extension] then extension = '.js'
    content = Bundler._extensions[extension](source, filename)
    deps = {}
    for depName in detective(content) when not deps[depName] #detective can return doubles, avoid unnecesary computation
      deps[depName] = yield @_resolveModulePath(depName, filename)
    
    return {filename, content, deps} = {filename, content, deps}
  
  ###
    Returns bundled code 100% ready to be consumed by the browser
    Adds module system and all the necessary stuff 
    recursively bundles all the modules starting from entry and adds them too

    well, it's a'la browserify, webpack, brunch etc
    
    @param {String}
    @return {String}
  ###

  bundleMain: (entry)-> async =>
    #First let's bundle module system, which will run in browser immediately adding "Module" to the global scope    
    bundled = (yield @getModuleSpec('apphire-bundler/lib/module')).content + '\n'    
    
    #Then let's bundle some global shims
    #Module globals-top contains only variables definitions. We need to define all of the globals on top
    #of our main function since the lexical scope of JS. Real assignment of this variables will happen down below
    bundled += (yield @getModuleSpec('apphire-bundler/lib/globals-top')).content + '\n'
    
    #Now let's bundle all the necessary dependencies (they will be wrapped and stored in Module._bundled)      
    modules = getModuleSpecReq entry 
    
    #Don't forget about the module, which stores global variables assignment (because it also requires some stuff)
    globalsMainModules = yield getModuleSpecReq 'apphire-bundler/lib/globals-main'
    for moduleName, moduleSpec of globalsMainModules
      modules[moduleName] ?= moduleSpec           
    
    for moduleName, moduleSpec of modules
      bundled += "Module._bundled['#{moduleName}'] = #{bundleSingleModule(moduleSpec)}\n"
    
    #Let's resolve modue with globals definitions now 
    globalsMain = yield @getModuleSpec 'apphire-bundler/lib/globals-main'

    #And simply require it
    bundled += "Module._load('#{globalsMain.filename}')\n"

    #Phew... finally let's require main entry point 
    main = yield @getModuleSpec entry
    bundled += "Module._load('#{main.filename}')\n"   
    return bundled  
  
  registerExtension: (ext)->
    @_extensions[ext.name] = ext.fn.bind @
  

  _builtins: require './_builtins'
  
  _resolveLookupPaths: (request, from) ->      
    reqLen = request.length
    # Check for relative path
    if request.length < 2 or request.charCodeAt(0) != 46 or request.charCodeAt(1) != 46 and request.charCodeAt(1) != 47
      paths = Bundler._nodeModulePaths(path.dirname(from or '/'))     

      # Maintain backwards compat with certain broken uses of require('.')
      # by putting the module's directory in front of the lookup paths.        
      if request is '.'
        if from
          paths.unshift path.dirname(from)
        else
          paths.unshift path.resolve(request)
      return paths
    else if not from        
      return mainPaths = ['.'].concat(Bundler._nodeModulePaths('.'))
    else
      return [ path.dirname(from) ]
  
  _nodeModulePaths: (from) ->    
    from = path.resolve(from) # guarantee that 'from' is absolute.
    if from is '/' then  return [ '/node_modules' ]
    paths = []
    p = 0
    last = from.length
    i = from.length - 1
    while i >= 0
      code = from.charCodeAt(i)
      if code == 47
        if p != nmLen
          paths.push from.slice(0, last) + '/node_modules'
        last = i
        p = 0
      else if p != -1
        if nmChars[p] == code
          ++p
        else
          p = -1
      --i
    # Append /node_modules to handle all the root paths
    for p in Bundler.rootPaths when from.indexOf(p) isnt 0 #We exclude rootPaths which already has been processed by earlier cycle
      paths.push(p + '/node_modules')
    paths.push '/node_modules' #finally add to main root path
    return paths.concat(Bundler.rootPaths)

  
  _resolveModulePath: (request, from) ->  async =>      
    if from then debug "Resolving path of module #{request}, requested from #{from}"
    else debug "Resolving path of module #{request}"

    if Bundler._builtins[request]
      debug "Resolving path of native module #{request}"
      request = Bundler._builtins[request]
      paths = ['/node_modules']
    else
      paths = @_resolveLookupPaths(request, from)
    debug "Lookup paths are #{paths}"

    if path.isAbsolute request
      paths = [ '/' ]
    
    if not paths or paths.length is 0
      throw new Error "Cannot find module #{request}"

    exts = Object.keys(Bundler._extensions)
    trailingSlash = request.length > 0 and request.charCodeAt(request.length - 1) == 47
    filename = undefined
    
    for curPath in paths
      if curPath and (yield stat(curPath)) < 1 then continue       
      basePath = path.resolve(curPath, request)
      #debug 'basePath is ' + basePath
      if not trailingSlash
        rc = yield stat(basePath)
        if rc == 0 #File
          filename = path.resolve(basePath)
        else if rc == 1# Directory.           
          filename = yield tryPackage(basePath, exts)
        if !filename # try it with each of the _extensions           
          filename = yield tryExtensions(basePath, exts)
      if not filename
        filename = yield tryPackage(basePath, exts)
      if not filename # try it with each of the _extensions at "index"
        filename = yield tryExtensions(path.resolve(basePath, 'index'), exts)      

    if not filename
      throw new Error "Cannot find module #{request}"
    
    debug "Resolved path is #{filename}"
    return filename
  
  _extensions:
    '.js': (content, filename) ->        
      return stripBOM(content)
     
    '.json': (content, filename) ->
      try
        return "module.exports = JSON.parse(decodeURIComponent('" + encodeURIComponent(content).replace(/'/g, "\\'") + "'));\n"
      catch err
        err.message = filename + ': ' + err.message
        throw err
                
            
stripBOM = (content)->
  if content.charCodeAt(0) is 0xFEFF then content = content.slice(1)     
  return content
debug = (set)->
  if Bundler.debug
    console.log set  
hasOwnProperty = (obj, prop) ->
  Object::hasOwnProperty.call obj, prop

stat = (curPath)-> async ->
  return yield Bundler.vfs.internalStat(curPath)      
readPackage = (requestPath) -> async ->
  jsonPath = path.resolve(requestPath, 'package.json')
  return false if (yield Bundler.vfs.internalStat(jsonPath)) isnt 0 
  json = yield Bundler.vfs.readFile(jsonPath)

  try
    pkg = JSON.parse(json)
    if typeof pkg.browser is 'string' #TODO handle cases when browser field is a replacer hash, see here https://github.com/defunctzombie/package-browser-field-spec 
      mainPath = pkg.browser 
    else
      mainPath = pkg.main
    pkgMainPath = mainPath
  catch e
    e.path = jsonPath
    e.message = 'Error parsing ' + jsonPath + ': ' + e.message
    throw e
  return pkgMainPath

tryPackage = (requestPath, exts) -> async ->
  pkgMainPath = yield readPackage(requestPath)
  return false if not pkgMainPath     
  filename = path.resolve(requestPath, pkgMainPath)
  return yield tryFile(filename) or yield tryExtensions(filename, exts) or yield tryExtensions(path.resolve(filename, 'index'), exts)

tryFile = (requestPath) -> async ->
  return (yield stat(requestPath)) == 0 and path.resolve(requestPath)

tryExtensions = (p, exts, isMain) -> async ->
  for ext in exts
    filename = yield tryFile(p + ext)
    if filename then return filename
  return false

nmChars = [ 115, 101, 108, 117, 100, 111, 109, 95, 101, 100, 111, 110 ]
nmLen = nmChars.length

getModuleSpecReq = (request, modules)-> async ->
  modules ?= {}
  spec = yield Bundler.getModuleSpec(request)
  
  if not modules[spec.filename]
    modules[spec.filename] = spec
    for depName, depResolved of spec.deps
      yield getModuleSpecReq(depResolved, modules)
  return modules      

wrapperTemplate =
  start: '(function wrapper(exports, require, module, __filename, __dirname){\n'
  end: '\n})'

bundleSingleModule = (moduleSpec)->
  debug "Bundling module at file #{moduleSpec.filename}"
  wrapped = wrapperTemplate.start +  moduleSpec.content + wrapperTemplate.end

  depsString = ''
  for depName, moduleFilename of moduleSpec.deps
    depsString += "'#{depName}':'#{moduleFilename}',"     
  #bundled = bundled.slice(0, -1) #remove last semicolon
  bundled = "[#{wrapped},{#{depsString}}];\n"
