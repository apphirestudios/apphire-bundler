do ->
  splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/
  splitPath = (filename) ->
    splitPathRe.exec(filename).slice 1
  utils =  
    makeRequireFunction: ->
      Module = @constructor
      require = (path)=>
        return @require(path)
      require.resolve = (request)=>
        return Module._resolveFilename(request, @)
      require.cache = Module._cache
      return require
    path:
      dirname: (path) ->
        result = splitPath(path)
        root = result[0]
        dir = result[1]
        if !root and !dir          
          return '.' # No dirname whatsoever
        if dir          
          dir = dir.substr(0, dir.length - 1) # It has a dirname, strip trailing slash
        return root + dir

  class Module    
    debug = (set)->
      if Module.debug
        console.log set  
    
    @_cache: {}
    @_bundled: {} #This will store prebundled and wrapped functions ready to be called by module.load
    constructor: (@filename, @parent)->
      @loaded = false
      @exports = {}
      
    require: (path) ->
      if not path? then throw new Error 'Missing path'
      if typeof path isnt 'string' then throw new Error 'Path must be a string'
      Module._load path, this

    @_resolveFilename: (request, parent)->
      if parent
        deps = Module._bundled[parent.filename][1]
        if not deps[request] then throw new Error "Module #{parent.filename} requested module #{request} which was " +
          "not prepared. You should call require passing module name DIRECTLY as a string. You can't pass variable " + 
          " containing module name. Also you can't use eval like eval(require(\'src/my-module\'))."
        return deps[request]
      else
        if not Module._bundled[request] then throw new Error "Module #{request} not found."
        return request
    
    @_load: (request, parent)->
      filename = Module._resolveFilename(request, parent)
      if Module._cache[filename]
        debug "Module #{filename} has been already loaded, using cached version"
        return Module._cache[filename].exports

      module = Module._cache[filename] = new Module(filename, parent)
      threw = true
      try
        module.load()
        threw = false
      finally
        if threw then delete Module._cache[filename]        
      return module.exports
  
    load: ()->
      debug "Loading module #{@filename}"
      if @loaded then throw new Error "Module #{@filename} already loaded"
      dirname = utils.path.dirname(@filename)
      req = utils.makeRequireFunction.call @
      args = [@exports, req, @, @filename, dirname]
      @loaded = true
      return Module._bundled[@filename][0].apply(@exports, args)

  
  if typeof module is "object" and module.exports then module.exports = Module else this.Module = Module


  

  