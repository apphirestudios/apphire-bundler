CoffeeScript = require 'coffee-script'
btoa = require "btoa"
module.exports =
  name: '.coffee'
  fn: (content, filename) ->  
    c = CoffeeScript.compile content, options =
      sourceMap: @sourceMaps
      inline: true
      bare: true
    if @sourceMaps
      return c.js + "\n//# sourceMappingURL=data:application/json;base64," + (btoa(unescape(encodeURIComponent(c.v3SourceMap)))) + "\n//# sourceURL=" + filename;  
    else  
      return c