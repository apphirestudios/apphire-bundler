path = require 'path'
cwd = path.resolve('.') + '/'

describe 'Apphire-Bundler', ->
  describe 'Requiring...', ->
    it 'requires from server', ->
      bundler = require '../index'
      bundler.should.be.an 'object'

    it 'requires from client', ->
      global.window = {}
      bundler = require '../index'
      bundler.should.be.an 'object'
      global.window = undefined

  describe 'Initializing...', ->
    it 'require bundler', ->
      bundler = require '../index'
          
    it 'throws when no VFS set', ->
      bundler = require '../index'
      yield throws bundler.getModuleSpec('./src-test'), /has not been registered/
      
    it 'sets server-side VFS', ->
      bundler = require '../index'
      bundler.vfs = require '../lib/vfs-node'

    it 'throws when root path was not set for VFS', ->
      bundler = require '../index'
      bundler.vfs = require '../lib/vfs-node'
      yield throws bundler.getModuleSpec('./src-test'), /VFS root path has not been set/

  describe 'Module resolving...', ->
    bundler = undefined
    it 'BEFORE: INIT', ->
      bundler = require '../index'
      #bundler.debug = true
      vfs = require '../lib/vfs-node'
      vfs.rootPath = cwd
      bundler.vfs = vfs
    
    it 'common resolve spec', ->       
      spec = yield bundler.getModuleSpec "./src-test/common"
      entry = '/' + path.relative(cwd, './src-test/common.js')
      spec.filename.should.equal entry
      spec.content.should.equal "module.exports = {ok: 'COMMON'}"
      spec.deps.should.be.an 'object'

    it 'resolves by exact filename', ->       
      spec = yield bundler.getModuleSpec "./src-test/by-exact/test.js"
      spec.content.should.equal "module.exports = {ok: 'EXACT'}"
      spec.should.be.an 'object'

    it 'resolves by exact modulename', ->       
      spec = yield bundler.getModuleSpec "./src-test/by-exact/test"
      spec.content.should.equal "module.exports = {ok: 'EXACT'}"
      spec.should.be.an 'object'

    it 'resolves by index', ->       
      spec = yield bundler.getModuleSpec "./src-test/by-index"
      spec.content.should.equal "module.exports = {ok: 'INDEX'}"
      spec.should.be.an 'object'

    it 'resolves by package', ->       
      spec = yield bundler.getModuleSpec "./src-test/by-package"
      spec.content.should.equal "module.exports = {ok: 'PACKAGE'}"
      spec.should.be.an 'object'  
    
    it 'throws resolving not supported extension', ->       
      yield throws bundler.getModuleSpec "./src-test/custom-ext", /Cannot find module/

    it 'resolves custom extension', ->
      bundler.registerExtension require '../lib/extensions/coffee'    
      spec = yield bundler.getModuleSpec "./src-test/custom-ext"
      eval spec.content
      module.exports.ok.should.equal 'COFFEE'
      delete bundler._extensions['.coffee']
        
  describe 'Bundling main...', ->
    bundler = undefined
    it 'BEFORE: INIT', ->
      bundler = require '../index'
      #bundler.debug = true
      vfs = require '../lib/vfs-node'
      vfs.rootPath = cwd
      bundler.vfs = vfs
      bundler.registerExtension require '../lib/extensions/coffee'    
    it 'bundles main', ->
      #TODO - make mock for this test to pass. Now resolver tries to find some necessary modules in apphire-bundler dir, which
      #will work fine in modules requiring the bundler but fails here since the test is actually the part of module itself in terms of
      #node module system. We could use npm link but this seems kinda hacky workaround

      #bundled = yield bundler.bundleMain('./main/app')
      #bundled.should.be.a 'string'

  