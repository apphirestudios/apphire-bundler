yargs = require('yargs')
	.usage('Usage: apphire-bundler --in <file> --out <file>')
    #.command('bundle', 'Bundle and create package ready to be consumed by browser')
    .example('apphire-bundler --in ./lib/app.coffee --out ./public/app.js', 'Take ./lib/app.coffee, bundle it with all of the dependencies and write to ./public/app.js')
    .demand(['in', 'out'])
    .help('h')
    .alias('h', 'help')
    .epilog('copyright 2015')
    .argv

console.log yargs


